var input = [];
var current = 0;
var totalStr = "";
var count = 0;

$(document).ready(function () {
    $("#7").click(function () {
        count++;
        current = "7";
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        //$("h3").html("<h3 class=\"header output\">" + totalStr + "</h3> ");
        $("h3").html(totalStr);
    });

    $("#8").click(function () {
        current = "8";
        count++;
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        $("h3").html(totalStr);
    });

    $("#9").click(function () {
        current = "9";
        count++;
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        $("h3").html(totalStr);
    });

    $("#divide").click(function () {
        count++;
        current = " / ";
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        $("h3").html(totalStr);
    });

    $("#4").click(function () {
        current = "4";
        count++;
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        $("h3").html(totalStr);
    });

    $("#5").click(function () {
        current = "5";
        count++;
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        $("h3").html(totalStr);
    });


    $("#6").click(function () {
        count++;
        current = "6";
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        $("h3").html(totalStr);
    });

    $("#multiply").click(function () {
        count++;
        current = " * ";
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        $("h3").html(totalStr);
    });

    $("#1").click(function () {
        count++;
        current = "1";
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        $("h3").html(totalStr);
    });

    $("#2").click(function () {
        count++;
        current = "2";
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        $("h3").html(totalStr);
    });


    $("#3").click(function () {
        count++;
        current = "3";
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        $("h3").html(totalStr);
    });


    $("#minus").click(function () {
        count++;
        current = " - ";
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        $("h3").html(totalStr);
    });

    $("#period").click(function () {
        count++;
        current = ".";
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        $("h3").html(totalStr);
    });

    $("#0").click(function () {
        count++;
        current = "0";
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        $("h3").html(totalStr);
    });




    $("#plus").click(function () {
        count++;
        current = " + ";
        input.push(current);
        if (totalStr == 0)
            totalStr = current;
        else
            totalStr += current;

        $("h3").html(totalStr);
    });

    $("#clear").click(function () {
        current = 0;
        totalStr = 0;
        input = [];
        count = 0;

        $("h3").html(totalStr);
    });

    $("#equals").click(function () {
        current = "=";
        var rslt = parseTotalStr(totalStr);
        $("h3").html(rslt);
        count = 0;
        totalStr = rslt;
    });

});

function parseTotalStr(input) {
    input = input.split(" ");

    var rpn = convertToRpn(input);

    var rslt = evaluateRpn(rpn);
    return rslt;
}

function evaluateRpn(rpn) {
    var rslt = 0;
    while (rpn.length != 0) {
        var first = 0;
        if (rslt == 0)
            first = parseFloat(rpn.shift());
        else
            first = rslt;
        var last = parseFloat(rpn.shift());
        var op = rpn.pop();
        switch (op) {
            case "+":
                if (rslt == 0) {
                    rslt = first + last;
                } else
                    rslt += last;
                break;
            case "-":
                if (rslt == 0) {
                    rslt = first - last;
                } else
                    rslt -= last;
                break;
            case "*":
                if (rslt == 0) {
                    rslt = first * last;
                } else
                    rslt *= last;
                break;
            case "/":
                if (rslt == 0) {
                    rslt = first / last;
                } else
                    rslt /= last;
                break;
        }
    }
    return rslt;
}

function convertToRpn(input) {
    var opQueue = [];
    var opStack = [];

    for (var i = 0; i < input.length; i++) {
        if (Number.isFinite(parseFloat(input[i]))) {
            opQueue.push(parseFloat(input[i]));
        } else {
            opStack.push(input[i]);
        }
    }
    for (var j = 0; j <= opStack.length; j++) {
        opQueue.push(opStack.pop());
    }
    return opQueue;
}